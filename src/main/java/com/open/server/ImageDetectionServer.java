package com.open.server;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.tiia.v20190529.TiiaClient;
import com.tencentcloudapi.tiia.v20190529.models.AssessQualityRequest;
import com.tencentcloudapi.tiia.v20190529.models.AssessQualityResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author tanyongpeng
 * <p>图片质量检测服务</p>
 **/
@Component
@Slf4j
public class ImageDetectionServer {

    @Value("${tencentcloudapi.secretId}")
    private String secretId;
    @Value("${tencentcloudapi.secretKey}")
    private String secretKey;
    @Value("${tencentcloudapi.image.endpoint}")
    private String endpoint;
    @Value("${tencentcloudapi.image.region}")
    private String region;

    /**
     * 图片检测服务，查看质量分，官方文档：
     * @param imgUrl
     * @return
     */
    public Long detection(String imgUrl){
        try{
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint(endpoint);
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            TiiaClient client = new TiiaClient(cred, region, clientProfile);
            AssessQualityRequest req = new AssessQualityRequest();
            req.setImageUrl(imgUrl);
            AssessQualityResponse resp = client.AssessQuality(req);
            return resp.getAestheticScore();
        } catch (TencentCloudSDKException e) {
            e.printStackTrace();
            return null;
        }
    }

}
