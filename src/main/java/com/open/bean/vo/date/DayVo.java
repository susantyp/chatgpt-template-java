package com.open.bean.vo.date;

import lombok.Data;

/**
 * @author page-view
 * <p>des</p>
 **/
@Data
public class DayVo {

    private Integer d1;
    private Integer d2;
    private Integer d3;
    private Integer d4;
    private Integer d5;
    private Integer d6;
    private Integer d7;
    private Integer d8;
    private Integer d9;
    private Integer d10;
    private Integer d11;
    private Integer d12;
    private Integer d13;
    private Integer d14;
    private Integer d15;
    private Integer d16;
    private Integer d17;
    private Integer d18;
    private Integer d19;
    private Integer d20;
    private Integer d21;
    private Integer d22;
    private Integer d23;
    private Integer d24;
    private Integer d25;
    private Integer d26;
    private Integer d27;
    private Integer d28;
    private Integer d29;
    private Integer d30;
    private Integer d31;

}
