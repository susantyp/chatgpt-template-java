package com.open.mapper;

import com.open.bean.LoginRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【login_record(登录记录)】的数据库操作Mapper
* @createDate 2023-05-06 15:18:23
* @Entity com.open.bean.LoginRecord
*/
public interface LoginRecordMapper extends BaseMapper<LoginRecord> {

}




