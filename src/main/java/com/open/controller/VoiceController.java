package com.open.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.open.annotation.Limit;
import com.open.bean.IpWhite;
import com.open.bean.OpenDict;
import com.open.bean.RequestRecord;
import com.open.bean.vo.OpenVo;
import com.open.component.JwtComponent;
import com.open.result.OpenResult;
import com.open.server.VoiceServer;
import com.open.service.IpWhiteService;
import com.open.service.OpenDictService;
import com.open.service.RequestRecordService;
import com.open.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class VoiceController {

    @Autowired
    VoiceServer voiceServer;

    @Autowired
    OpenDictService openDictService;

    @Autowired
    IpWhiteService ipWhiteService;

    @Autowired
    JwtComponent jwtComponent;

    @Autowired
    RequestRecordService requestRecordService;

    @PostMapping("/start/voice")
    @Limit
    public OpenResult voice(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        if (StrUtil.isBlank(jsonObject.getStr("voiceBase")) ||
                jsonObject.getLong("voiceBaseLength") == null ||
                StrUtil.isBlank(jsonObject.getStr("viceType"))){
            return OpenResult.error("参数错误");
        }
        OpenDict openDict = openDictService.lambdaQuery()
                .eq(OpenDict::getDictCode, "100003").one();

        if (Integer.parseInt(openDict.getDictValue()) != 0){
            return OpenResult.error(openDict.getDictDes());
        }
        String wId = jwtComponent.getUserWId(request);

        IpWhite ipWhite = ipWhiteService.lambdaQuery().eq(IpWhite::getWid, wId).one();

        if (ipWhite.getRequestCount() <= 0) {
            return OpenResult.error("你的余额已经用完！如果想继续可加群聊：237343691，联系群主");
        }

        Integer userCount = requestRecordService.lambdaQuery()
                .eq(RequestRecord::getWid, wId)
                .last("AND TO_DAYS(request_time) = TO_DAYS(NOW())").count();

        if (userCount >= ipWhite.getLimitationCount()){
            return OpenResult.error("超出限额使用，具体查看首页 我的账户");
        }

        OpenVo voice = voiceServer.voice(jsonObject.getStr("voiceBase"), jsonObject.getLong("voiceBaseLength"),
                jsonObject.getStr("viceType"));
        log.info("录音结果=========={}",voice);
        if (voice.isState()){
            return OpenResult.success(voice.getMsg());
        }else {
            return OpenResult.error(voice.getMsg());
        }
    }
}
