package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.UserUpgradation;
import com.open.service.UserUpgradationService;
import com.open.mapper.UserUpgradationMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【user_upgradation】的数据库操作Service实现
* @createDate 2023-06-07 16:32:22
*/
@Service
public class UserUpgradationServiceImpl extends ServiceImpl<UserUpgradationMapper, UserUpgradation>
    implements UserUpgradationService{

}




