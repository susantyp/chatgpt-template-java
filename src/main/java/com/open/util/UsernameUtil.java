package com.open.util;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public class UsernameUtil {

    public static String getUsername(String ip) {
        try {
            String[] ipParts = ip.split("\\.");
            int sum = 0;
            for (String part : ipParts) {
                sum += Integer.parseInt(part);
            }
            int firstCharIndex = (sum % 26) + 65; // 65 is the ASCII code for 'A'
            int secondCharIndex = (firstCharIndex + 1 - 65) % 26 + 65;
            char firstChar = (char) firstCharIndex;
            char secondChar = (char) secondCharIndex;
            return String.valueOf(firstChar) + secondChar;
        }catch (Exception e){
            return "ME";
        }
    }

    public static void main(String[] args) {
        System.out.println(getUsername("113.204.21.234"));
    }

}
