package com.open.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Data
@NoArgsConstructor
public class OpenVo {

    private boolean state;

    private String msg;

    private String data;

    public OpenVo(boolean state, String msg) {
        this.state = state;
        this.msg = msg;
    }

    public OpenVo(boolean state, String msg, String data) {
        this.state = state;
        this.msg = msg;
        this.data = data;
    }
}
