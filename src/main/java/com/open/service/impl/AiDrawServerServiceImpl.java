package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.AiDrawServer;
import com.open.service.AiDrawServerService;
import com.open.mapper.AiDrawServerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author typsusan
* @description 针对表【ai_draw_server】的数据库操作Service实现
* @createDate 2023-05-26 22:45:22
*/
@Service
public class AiDrawServerServiceImpl extends ServiceImpl<AiDrawServerMapper, AiDrawServer>
    implements AiDrawServerService{

    @Autowired
    AiDrawServerMapper aiDrawServerMapper;

    @Override
    public List<AiDrawServer> getAiDrawServer(String wid) {
        return aiDrawServerMapper.getAiDrawServer(wid);
    }
}




