package com.open.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
public @interface RepeatSubmit {

    int time() default 1;

    String msg() default "请不要重复点击";
}
