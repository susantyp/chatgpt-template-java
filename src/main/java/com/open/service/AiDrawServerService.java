package com.open.service;

import com.open.bean.AiDrawServer;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author typsusan
* @description 针对表【ai_draw_server】的数据库操作Service
* @createDate 2023-05-26 22:45:22
*/
public interface AiDrawServerService extends IService<AiDrawServer> {

    List<AiDrawServer> getAiDrawServer(String wid);
}
