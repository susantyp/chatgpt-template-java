package com.open.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Data
public class RequestRecordVo {

    private Integer requestId;

    private String requestContent;

    private String respondContent;

    private String systemName;

    private Integer rid;

    private String modelType;

    public RequestRecordVo() {
    }

    public RequestRecordVo(String requestContent, String respondContent,Integer requestId,String systemName,Integer rid,String modelType) {
        this.requestContent = requestContent;
        this.respondContent = respondContent;
        this.requestId = requestId;
        this.systemName = systemName;
        this.rid = rid;
        this.modelType = modelType;
    }
}
