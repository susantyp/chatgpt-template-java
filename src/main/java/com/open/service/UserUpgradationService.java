package com.open.service;

import com.open.bean.UserUpgradation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【user_upgradation】的数据库操作Service
* @createDate 2023-06-07 16:32:22
*/
public interface UserUpgradationService extends IService<UserUpgradation> {

}
