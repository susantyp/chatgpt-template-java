package com.open.mapper;

import com.open.bean.FileCache;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【file_cache】的数据库操作Mapper
* @createDate 2023-08-15 17:01:43
* @Entity com.open.bean.FileCache
*/
public interface FileCacheMapper extends BaseMapper<FileCache> {

}




