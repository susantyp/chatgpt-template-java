package com.open.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author page-view
 * <p>des</p>
 **/
@Component
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowedHeaders("*")
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                .allowCredentials(true)
                .maxAge(3600);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加放行的路径
        String[] addPath = {"/**"};
        String[] excludePath = {"/request/open/reg/ip","/request/open/login/ai",
                "/request/open/generate/code/**","/request/open/generate/email/code/**",
                "/request/open/vef/email/code","/request/open/version/index/**",
                "/request/open/qrcode/url/**",
                "/request/open/admin/ht/login",
                "/request/open/user/get/share/chatting/**",
                "/request/open/update/password",
                "/request/open/selected/all/img",
                "/request/open/selected/his/works/**",
                "/request/open/ai/draw/img",
                "/static/**","/css/*.css",
                "/fonts/**","*.html","/*.html","*.ico","/*.ico/",
                "/js/**","/img/**","/*.ico","/*.css","/images/**"
        };
        registry.addInterceptor(new OpenAIConfig()).addPathPatterns(addPath).excludePathPatterns(excludePath);
    }

    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.addExposedHeader("ip");
        corsConfiguration.addExposedHeader("token");
        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", buildConfig());
        return new CorsFilter(source);
    }
}
