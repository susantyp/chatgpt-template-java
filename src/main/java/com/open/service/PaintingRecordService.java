package com.open.service;

import com.open.bean.PaintingRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.open.bean.vo.PaintingRecordVo;

import java.util.List;

/**
* @author typsusan
* @description 针对表【painting_record】的数据库操作Service
* @createDate 2023-05-27 11:43:03
*/
public interface PaintingRecordService extends IService<PaintingRecord> {

    List<Integer> getServerCount(Integer sid);

    List<PaintingRecord> getPaintingList(Integer sid,Integer serverType,Integer pageSize,Integer currentPage);

    List<PaintingRecordVo> getAllImg();

    List<PaintingRecordVo> getToImg(String wid);

    List<PaintingRecord> byDays();
}
