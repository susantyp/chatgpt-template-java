package com.open.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.open.bean.OpenSystemRole;
import com.open.bean.RequestRecord;
import com.open.component.JwtComponent;
import com.open.result.OpenResult;
import com.open.service.OpenSystemRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class OpenSystemRoleController {

    @Autowired
    OpenSystemRoleService openSystemRoleService;

    @Autowired
    JwtComponent jwtComponent;

    @GetMapping("/role/list")
    public OpenResult roleList(HttpServletRequest request){
        String wId = jwtComponent.getUserWId(request);
        List<OpenSystemRole> roleList = openSystemRoleService.listByRole();
        Map<String, List<OpenSystemRole>> listMap = roleList.stream().collect(Collectors.groupingBy(OpenSystemRole::getRoleClass));
        JSONArray array = new JSONArray();

        List<OpenSystemRole> systemRoles = openSystemRoleService.lambdaQuery()
                .eq(OpenSystemRole::getWid, wId).list();
        if (systemRoles.size() != 0){
            setValue("自定义角色",systemRoles,array);
        }

        for (Map.Entry<String, List<OpenSystemRole>> entry : listMap.entrySet()) {
            setValue(entry.getKey(),entry.getValue(),array);
        }

        return OpenResult.success(array);
    }

    @PostMapping("/role/save")
    public OpenResult save(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        String wId = jwtComponent.getUserWId(request);
        if (StrUtil.isBlank(jsonObject.getStr("name"))){
            return OpenResult.error("角色名称不能为空");
        }
        if (StrUtil.isBlank(jsonObject.getStr("desc"))){
            return OpenResult.error("角色描述不能为空");
        }
        OpenSystemRole systemRole = new OpenSystemRole();
        systemRole.setRoleName(jsonObject.getStr("name"));
        systemRole.setRoleValue(jsonObject.getStr("desc"));
        systemRole.setRoleStatus(0);
        systemRole.setCreateTime(new Date());
        systemRole.setWid(Integer.parseInt(wId));
        systemRole.setRoleIcon("data:image/webp;base64,UklGRiAbAABXRUJQVlA4WAoAAAAQAAAAjwEAjwEAQUxQSKcGAAABoABJsmnbmuf3Lp9t27b1bdu2bdu2bfPZto1777fVX2etffbZa6Y/T0RMgGT9n/V/1v9Z/2f9n/V/1v9/o8+/YenPxXfX+OvgBPz5VX8VnIzUd/w1kAPXQX8JvOKEiOq34hrcm0bxAADM7MWzBh4H+/VAyito1tDjFq+WcNyLZeU83vSCM8vkR7dNPru7nciypW7wWeE2jmUveRR4wH0jy87z6OJ2pMdalu3icbhbkcfjLGvjcbtTLjx7sEw8xjtd4SM0/8mt2Amed/JsqRtcWvvU59kzHvkOD3v8LDw/26OPAzwvIdoOHoemquqTT7SWHgemauixTpjuUTWVeBztN+aaFx/anVUfOv0gjiVu4rsb/vxQdvQ88YmJS9YXrR93z7FN41TGaTeXo5xe8HkKqU9nxqET4fly39jIQQ7viPPPLuK5Bq6tWdH4WURZvF9cZK8UT4hnqu/FvRvcv+FEmdcRdVGXmIhcswYbH20l3vf+yQ3ifi58cxlxNNL5cFyi7zq6pXi+C++BhBiH9G7IMO/Cz+E/gA45SH9hgoxClMLGqohjmcS4GlGuZ0MpxDMpFiDSvmxATD9KhKaI9h0h48y4oHsC7INoXxEy7oPYfpV5DyDaU4SNiHG7TFuFaHsKG6+O03sZhmi3CB8R68zaGs3Twscj43WCVKleWFguM/og0v2FkOvj5Vj08QNnbtcyXpMiqSeMRGZ/MfGYerFBhHOEkrtn2B9/eGynMplxs3DyuQT449Irm8ZvrJByY0IAWHdYusb7FAgrkahXlktLPbf3hJfJAjzfLA1yictZQjNgXufo5P5UPYRpwLPRSdd3Aaw8U7j5XQIBR0cmImWFnpMTCWsbR0fQi5MJeJw3DZMKGMEaSS7cx5pTkgurSfNSggEdCJP3LZL9WLocgMS/lSz3IYCPU2UcgvgiUZYjkK/TZBWC+RZJ1iOgb1NkM4J6F0HWI7Db0GMFgsuOqQgwN65DiF9lRj+EuRUxEGpelATrKFbciXCToi8C/iAnEPRcRnwYtlcJ0R2BJwRCfxkddgke6IDw70qGMxWwmgzQIBduUMHlVIAOmXCmEroSAUp8iwfttQAevKSGPWgANS5iwSF6AAuKFHE6B6pAkd9z4DRNoBkFxqviYQpAlwxopYwhBDhXGVMJMF0ZIAC0eaL52qujxHynqQOF1ntCH9dYb44+YL0fFNLPeFDom7arpxHYbpRKDjDdHipZbrrdVQLTba+TmVUNt51OgKf+cgCOsto2esF1RttFMbjdZrtrBrVNNlg1MFmebnaymOjma5NtVQ3qWexj3Txlsct0A4t1VM4eBhPlrLPYFN2gosHOUc5VBmuuHBhMtNPLYC8p52WDbascGEy0c6jBZihnqcEOVg7K2Eu0c6bB1iinxGBnKAe59srTzhH2kmLlvG6wS5QDgzVWzk8GE+WssdhtuhlnsTa62cdiohsx+eOaecVmwzQjRtdMvtHeVcwKo22vGDS2mWimyGhzFYObbHaEZrCTyUQ16GOy1apBE4udphs0MlgV5aCZvWS+ctDGXu9rB/2t1Qj63clYqxWEw0xVCSq+xFIn6gj3GmqukvCqnX7SEmaa6Qc1ocRK0LSRNmoK1Ux0i6pQx0J9dAULibIus9B4XW210EG6goXkr4d1qtpoostUda2JGqpKbKypqUZ6WFFi5L56OsJKoqaVYubXtSR23kFJYmkdNTHVRA3VF1Pvo6A8MbZ+yom1ZynnJ7H3DrrZKhZXzUIxeVXFvCxG30stZ4nZOytlqFj+RYVsLS22rzhLG7eL+dsqY6DYP1cV7wsFNbG9cHCzGl4SFk5TwncDhYZTdXCBEHGmBl4RKq4MX3Ef4eJPoftlZyFjGQT+VKFji7DdJoQcHrJbhZKnh+t2IeXTobpZaLkwTBcJMRHgn48VZuYGaE/hZo/wTBZynhieQex4KjzCzkXBuZoeCK6ws0pwDqTHqNCsF3peGhrh52uBOZQgn4ZlsxAUYRWCNgqLMHRAUISiOwTkR+FomXB8ISzdHIoFQtPqgThDiDo6o2YcNuq7WIyrJFRt+G1mFD19WFP548XpW9Va6HriZzHb/OoZ3cW19Jvp+WIHoWzv+4ri8enMB/duLhHmn7Mxssk7CG/Ljjnz4dnfRbZmwpOnb9tM0ln1/A0RfHRoKWFwzTbD9zv+wpsefvDuO2+57dKjdxnYrrrENGfQcVc8MWnrL8CWSXce3StH/iLMadi2SY5k/Z/1f9b/Wf9n/Z/1f9b/Wf9n/f8PaABWUDggUhQAAHCFAJ0BKpABkAE+MRiKRCIhp6IiENmI8AYJZW7jRoSy0AZKvszTJZY/8DsZPj+L/F39z/8/1jnPnf794v892NyWO1L+N92HzP/3Hq6/QX/O9wn9Kv7f+W3+F+K/9gPfd/UP9p6h/5b/X/9//g/3/+b3/aeqz/HeoB/gP8p1jvoJ+Wh+1/wjf1z/dftf8DP67/9H8/+4A///C+dbDmIN5O1r/ifyW5K3lX9e/0PqAzkvsph3jy9Gb/w82H1l7BP8z/sP+47FX7qez7+4AshkvODJecGS84Ml5wZLzgyXnBkvODJecGS84Ml5wZLzgyXnBkvODJecGS84Ml5wZLzgyXnBhDaQ5SYiUZ3wMN2jsf4DXGuNcZ49qWEl5IYGugbwLf4mdAE7dGms/TXUWApRmvjgL+3WQMkqbhu7ZBnwZHlu0AIlzJTF/QtoHKoLjar8PzVaeX1mnSh40Wu6W5+KMj1wBWcSui3w14QVeMtEVhccGS8knZCSkwov2Ore0rKQlu/nuJiCTkVw54jq0te23s02bDL2/7BmuxiH6L092c3oftSk0KN9z2rWcuq2VeZa84134gBNQoJBZgGmOyMN8WJqtoa4vBQ3K0OVocrQ5WuVhJcTrMgerqzi8/I62Pw6OEn9YfWwA9cCFNyhctD3CTmR7IQa0L5vXHjzo1qUxHWOdqUxiJMnUZqcAXMHT7ocDm2og1j8LYIw8GNMtmOtEVhqmSi+iMgDd2i9oK+TBkO4Na9VtX8hrVt4YJMol318DGrWESvzZSFpOCuRtklTcN6+ElwMPiqGm5jW1vAk2h1c55tGKWa9f0JNIkyvKgaDBcicg9iQppKM5JFVhqwkyjga5U/yeekB7UFyOIe7IZUqZt+waJ/+JY///15A4tPys3hmCT0vLGIkGc00B6sTNFTfovT3Vmk4FJ16x+/7355W2v7I/9a1X/pnw9Np2MWL4jgNHgS/z+eeJUesSCTOhCAtfxNTeoO7MpR8MsVhtwhUhytDpztSvrADudLAV8rZ2hH5YLAGTZmruuNocpQZWEZdfOZxB9sBnf/HlIg39wmfxmBbKQdfCDj+/xJJpYbgdh2A/9YxJwNddGurChyvmvRXphRrDemqelnVBpBL/n8MPnK/+XdnQW8YPBwyVMTvY6+iSCOtUsb+AuMuGiJuG7tQHvKOBrrIFTz/duprKYkXEKJbC/Uzlr3oSt4Hf4O++/GSpXu6R97XYntgEyOp4h/18JkrxloipuGfBhcWJ09VyhuQw10lJmxOq+YFfs+1PxOwEtCIyr1jFAmFz/D6dv4Qz9UIcrQ5WhytcrCS8noiDC2TpdqMuGGKf9JhTuBklTcN3agPdItCYnA110a8e1LCS84Ml5wZLzgyXnBkvODJecGS84Ml5wZLzgyXnBkvODJecGS84Ml5wZLzgyXnBkvODI4AAP7+teAAAAAAAAAof/49A1/5aI8Ewin/1OUpT8NYVz7wSVCkmqqwkq4QOXngQ/xs3b1lBylUE0iRQEd7Lmr2qBQf09fjPz1+FV6hipqMybEuFh38N3QIqL9MMhPl1Hfuw/zqgsVZ/ochnts1UUV/sGpuqnogmGjFYvJJrhQo1JEAWNSTC/SlsA3wb2Mtfkm8WxrNDKg5bOpQZjRrfYhFjdEAz+NyfTtL6p1hREt0eJdOmWjKRqSTCsyIVPPT2vDN71fvulK9S7HfL+mhqFusukyEvKmHtFHD9TK7bB/dTyFs7UkbsaXWEMVcpOR8glSWCWaESIRAXzKJ5iZys82oegasLyk3QDWBmaGQ/Pm0zL4vTzH9rkj7Q5fKOjFW/6UD//ms2eGKlZ3bjnvyMFaBRvAbOJ5f/Jc3m8ocSYG0Ags+0jCAW0+HjGbym/92WKfjp4T0tA0teVbxGOJWbVKjQk2bWIufgyVw5rKUC/rH90dxJA1dUqzfqp8ayv79g45oepRGQ1NG8ixqKgyUI7nzT7uSgJ1A5ILAMH4Bjqu+FKaXGFmRkSuf7lKhd8KEF5HiCPwg02OnEa2MAmk1u7Bsh6tFjnHzpLa+r4mYnLzwjUwAB1e22KzQc7ROdykMPN9sGpRl/ep7C8KUOjXatBICVVrjX2uvZVCI1uHFFoyIU76KMPgH886VvEEVJOL9eAsRXm1rNF0tMf0zOUN0+de5nW8acNO0UV04W97bI7OiAWHdaFKCW2Ps8cHDImp0d3rJIVx9NeFeho//4sLbrFrVxhLzIu1y/0yyjBh3dqBA6xWlZMVpSBjEYXBnf1vnsRqqBJGQ1y9HIN7T0deeSfEpbwTQswDxxOk62Apg29VQv5WrO5IQb/h5YpG0W1R6SMiROeVkw1jdHLDyvxqIIWDDTEWG490NS4LgDUL2gnbe0v1Cc62rM/kRDASmU9ZgiEbnD0smFPX1muHmC+60X9uaoX4ulSvBgDb9/jP2iPPenx/DWox7IIf8+A39naB/Ks5EvvoIzqjjO0MGp92FJ2qx7Wnun3l8OFTtYUKXtpOHXmceh+GPdM1N9iMbF3zikDOvoOZQKjRK1uPE4dcQkaAAEza+z9j3gEclxdliviQLu//QognygID2x2pu7CNr8FpGU5yaCHgmumnhpoosBi/l26ShzRKLT+CcpwQ/ygV04YgqHuMAtz+FI007N1WYQy7hCxA2dEgUReiq20ZWjDPx6E0Zdjm/kZ41EIPtvR3gU8z0h4ylMq8x5HmJpI4F5it/jsYOk5HnQrwO80LW0StxzZePUSgtiTGQcwRgSjPxEfI+QYA++8F//6wKfzcFhjxAqaARuhceX9DMB0iH5kPTM+P24udCp3A3Xvv1c7SAQke57QD8qATdbVehZsWPO7IKGbTGPUdxFtfHsCFOu8I10L6jdRUC4NgHk5fx1UrIDjkubnDzfJ0G6bbivKZQOmeePFbURxE5Gt4/PHwmoeP6cr3B5NPo2rf1I3aAtakBS9dTHNXAstbMg+XSwvrFe7+QUmnfzJBm2w6NwFfnNHNLg8Zg1vj9AK9S5ObWcE8GTJ+c6w0u40oB/vefgVAjl98LO+sv4MLtZdxXi3aY3X0yv+7Y7leVSu6+uXJ7N2csOM+m7PwqPhq8Dxi1vw1I3We27ost2LT0kLgDNYx9B7g3rtrPlZvT4Ej+X11HF//gdf3jHZYR4DmvT2qDNnKqSEsAyYxLKPdKt3u8/Gw/QBKRuVoguzUD0cuIBzncUsj9jujC9LZD3xZCBbYL0pZkg0XNXMYHTqZPkXmvrGvJUj3nuL3MFKmwxrNINUQA8ftPhoc/K7uJFlHjWsUK9fAqFBo5PuNdw5uDsXH8YqPfd+xP46KKJKmIFuG4ciimpzrihOu3Hn1dVPvV/aJHxQoDwzrSfQX5ZR7DUHMBb6X90btPEfS7WSwmTH45dDfchXB2lR2r15Y3ER1bbQDPOIMC+/GuRZG1HkB64lcPY/4T0P06DgaHSOKiffy6bGbe/VwOnBiOnjnH6xf8mEu1CkGR17MZk501s6m/D/wtce5Xt/xyGV/+CBagEQPw4NyEQqvuobz6lYBRga5LI+i0rWvCxrx6z2LwxJFzmuZj/jv40ZMDmZ7C986IYac9D93wYJ8LDR+8rvf6cLZEm4Lu8tkaVVNQ8jsWYZkFk4gYSf+VDaZ6UEcLBiB0xFb0S+WrTVjxMCdcWP78j+/bjeNsvVsPttRXn9MmEjx+wz13znd3bLqnMtzIZaD346XckqOnfNzAfzsw86C4EXamxk26srOEj/INywNEaLM/4IWveuODy/MA1U+JHgloTt8BLgeCd9M3BjZMn30ZiqBUJanvK7VK5rm/3UFooxauH76hv76Jqj5KH/k8yCPH4zUG1suG8hNo5kl2UJ3iiMEnKZDz9iezNeDDmlGq+C3kDtzYSws+DBl4EdeoeNBz0u0l8/Xc/TlgaelG1gT8huwwmBOcj2gk1ZCe1ocwO/jpu5EtXcOegelZDOvQNLUwCqguT3clf4E4iaMlKxZeEGPqF/mLOIXS1kYg5Ah0vq4HiLTuOfk5TT0oKtlLwXmVdX7H//FhSH/hLd/37Msu1t6hotUjnNH6QtPPh66LtwBkM5KqwdH0EbtIbfPJKLY/MbTTdavSCVmWukrzp6r8iIbDc9dNO5+w/vfZdiIb75tjiVp+/owGk4zp1Hx0BTd75UFUgvQWAuV8ETchm81ucmhAhKBkjXxA7QiJc4E+9xTg974di0Y3YF5LW1OKWvLJxP1qoY3S97IaSliUmfjSAyRl1fKtzqNu2b/+X5xa31v8mBxFGnKfDfvN4WkWSaPTz2gVkFN8M2NCAAVduRFX4c4dnhrDbm0hbWhMwapaiGcWL3Z6hU6b1JoW0nbWeVkDLeT/mPDcPpUo+egCNof5FYXnrbKNC1MGSwV9o6QhfLiBuL+pPdqLV097ACHYLlw6LsdwpZ4+2E2i8i5ik6E7AsNKNhqdw0dmcxmuKu7npzW3s9bgkYpghzRa+JO+TeX3az1/8Cp9wAwBYFiGZBFbYmnKwO8Q+9kqXifn29X3mEs4UYZJ8iQjYBbLs38VfBC/satCO4bMAhLHWR2W/1X4O/oCNQVoobiynU3B5kt9FB96BkbMrPxcqvyXXyvOl9PQ+L8/v01biw/fc3O3J9J0a0qxIcQNYQOPhS4LZVim+SwwwqdjZ5sFZ5M1nE5IRf/czVw05v/fmeRfr/Xtp4infZYNlsd1li4BENaf/yF1sRRxNyeV4fDRxTqcruIgAWUG0Jnx2n4YCqa1jNwVEAXRZM7/twBxCu8MHzaXsnOHhWGh9SoeTOaaHUJ9NjhIgOTLGcve8nXsJ+Q6PVb0HMpiRBfivd/6rs0bsIVADufw303pu78uYx4NyxYXe7vPeTqS0kuNtnDBn0HowRihsciMtbS3TF/y336mhZXJ+YtQdEzDvVU4f6WgW+wxMgc5rG4E3z//u6yqGrtc2tzmtnXP3EReCC1eBIKZVJUxGw7ndNLddvpKvnMDSKNhtjamBmylnjomixlboEOoqqOzZLBcht+hM9rE3LJ2whiREgiAdPThDxN4+Qt1dW3GTHElNMumIZFo/8LhL+EZMlQv5TzhCmlZaP0hbp2nelCPWA5NPl2Hi4mYtD6Ogp4GVxEnSBj1DFNG6iLzHcp8OhBKADBRE1ul78sIezZXvI+5SfsDQzk3CExsb+RhWvWg9NsDSImPM1ekWgL9ksV1ONc7q+La6RDnsTSy4vwTPEhgWEkpq5R7A9hgDVunRcmBHmZsWEnAWD/UuNU8FBf5aPf7e+P9aVwtg4nsLOBmNvUBCBKx8/AuJ39NSWCAam61RFwEFWc2kXV4kAOPcIKOcLI51n8LO5HgfGsoJc/bncRVBeYITKzwiLFyQPfE6PpQn0A0sTXr/8Szq0C1od9T9s80TiPuMNjxWv2cPxxlp6cKjaInXMNu9u6e1mYMqXEknBTPMwY1hQ9CM+yrdGLEHa7b0s0Zzv1eVEUrDXkm8Sr4gUvSSBgg4yDe0Z9MHq3mChW8/v/1H35TS6/h9XJxOhbClytxKmq3yw1wpa+0vrVpM7yFDHR7IVvSZIzoZvFIesHN/xGnw55f5ZTtOC/iKCw4ygUqx0v8hPO1XhTBbiktKf/9l7JBBPPTqOz5ykkKINyAtNAy0/8f5agr+X8mKqEd8mlnmCz0dwbGnStzRl9Lh79eR8ZkNMVDfxlAi3uMS6zO3AlNAOxmb72gQshA1ZjQ1DPp9n0E2mlYQPkzSrDCZWC6Uk5QkcWt6/GqUwGAOtBBtP/Jpvcq2wvp0H/K9/GPFVzlfbW1afV2vCnTZ9efuik3FXDJCAtUVtynCzCugs35l44/E7XixvLSWWHxh2Og3UTIXoPdQiZepwAbTcbMdW8s2spOsCeDGWDtwA8isvh3x4CHRnrqQNkxOMFaFKXouhnnLOYQ2ar2m/ZWik5wnY/e1opc3gj2pjnc0JwnY/msuxIXgNfTeyeEnQB0AMC0lGvspsPQTk9TFN/w1ukUK/NN/zkSUXcPw58vMA5ZgQOUTVRNKro64IvjQ1wTY6pcjSKwTCLq/o803J7xwj0IyqI3bX1fJckUHhidWl8NsXGwLofOPPkfJkooTaa4qFAZcjwY+4t3NdjvuPbjRd76lq2D1XhxIs6w+jUqvN6U01ML0ljSxqufsgJArxgc1Ca6AVVSLuSwPa3rHAsKIy3t4ULygQcsd8A8Md7W+rvlQszPTPWS83S4ow/zUMUWwTH2be4Dil+y6H/nMgASHit9GRXlXfprYhEulOSnslk5zlm/pO5SjAzXioI+7aMvMjrhT3K7+ad27ytBMkhn+q1N+XpTaOwyLfehS+EU0vTyXglUJqiP/QEkaQI21eMgYKaVcP4S3jWeukB14H15tc9x+aTin3UP4MfqaHzaPF1glNwWICA9FVk5LRkJxx/0LKrJKF5AzpPdsl1mfj2mmJ5IGWTPqCuPHIr4G1TwijKwDQoBsvfpkWJCeoOJmPmKOMIsCfcVzXsXglgZ0J/mYI2Y7AmS/fu8IC4EljE+r6dAxVEnwYyVENACLNmJSBWN8W4GO4Aj08akNi3gHToi+Cq/t93dqehVGwbQCe5gBxKgZIWIhB4Dg01TTAPjJrYFiaqA///QAjhQ+mBRkRW6gBL943ECaHnTfM7ubBEb3dk4C8Xwi2frsQCkWW/h8CTmqYD4O/sLHhsdPi19qlE2xCQPIDyi0bar/1JwM4cqeceeVLG5uWMJwtu6mgeUI71qqsa8/2f0zz8aLUVYUmpI1T0g0wYsrxRozlWeEawTtpHWNQNTT0jVuSnPTrzM0dBsBD389C+HGiiMvHj2SDrihGTSYRcepuAA3e6//3Tv2xaxuM/4IJpgUlP0YmMftdnYQSbjXDAcBLUQxumfivIP/+wL2AJSd4b1rk+uPLjLGYVfH9o9RooM+P3cskDSm2vhbCsfUAAAAAAAAAAAAAAAAA==");
        systemRole.setRoleType(1);
        boolean save = openSystemRoleService.save(systemRole);
        return OpenResult.save(save);
    }

    public void setValue(String className,List<OpenSystemRole> openSystemRoleList,JSONArray array){
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("class",className);
        jsonObject.set("list",openSystemRoleList);
        array.add(jsonObject);
    }

}
