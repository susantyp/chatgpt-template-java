package com.open.mapper;

import com.open.bean.DistributionRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【distribution_record】的数据库操作Mapper
* @createDate 2023-06-29 16:14:23
* @Entity com.open.bean.DistributionRecord
*/
public interface DistributionRecordMapper extends BaseMapper<DistributionRecord> {

}




