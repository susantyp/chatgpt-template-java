package com.open.exception;

/**
 * @author page-view
 * <p>des</p>
 **/
public class WebException extends Exception{

    public WebException(String message) {
        super(message);
    }
}
