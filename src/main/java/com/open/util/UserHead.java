package com.open.util;

import java.util.Arrays;

/**
 * @author typsusan
 * <p>des</p>
 **/
public class UserHead {

    public static String getUserHeadImg() {
        int num = RandomUtil.getRandom().nextInt(50) + 1;
        return "https://plumgpt.com/chat/default/" + num + ".jpg";
    }

}
