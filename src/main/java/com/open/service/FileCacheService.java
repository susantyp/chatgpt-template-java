package com.open.service;

import com.open.bean.FileCache;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【file_cache】的数据库操作Service
* @createDate 2023-08-15 17:01:43
*/
public interface FileCacheService extends IService<FileCache> {

}
