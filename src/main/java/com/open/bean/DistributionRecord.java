package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName distribution_record
 */
@TableName(value ="distribution_record")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DistributionRecord implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer did;

    /**
     * 分销人
     */
    private Integer wid;

    /**
     * 分销创建的账号
     */
    private String username;

    /**
     * 分销创建的密码
     */
    private String password;

    /**
     * 分销的额度
     */
    private Integer requestCount;

    /**
     * 分销时间
     */
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}