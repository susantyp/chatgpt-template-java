package com.open.service;

import com.open.bean.RequestToken;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【request_token】的数据库操作Service
* @createDate 2023-04-03 17:56:30
*/
public interface RequestTokenService extends IService<RequestToken> {

}
