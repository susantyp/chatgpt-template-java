package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.FunctionUpdate;
import com.open.service.FunctionUpdateService;
import com.open.mapper.FunctionUpdateMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【function_update(功能点更新表)】的数据库操作Service实现
* @createDate 2023-07-26 16:41:47
*/
@Service
public class FunctionUpdateServiceImpl extends ServiceImpl<FunctionUpdateMapper, FunctionUpdate>
    implements FunctionUpdateService{

}




