package com.open.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.open.annotation.Limit;
import com.open.annotation.RepeatSubmit;
import com.open.bean.AiDrawServer;
import com.open.bean.IpWhite;
import com.open.bean.PaintingRecord;
import com.open.component.JwtComponent;
import com.open.result.OpenResult;
import com.open.service.AiDrawServerService;
import com.open.service.IpWhiteService;
import com.open.service.PaintingRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class AiDrawServerController {

    @Autowired
    AiDrawServerService aiDrawServerService;

    @Autowired
    JwtComponent jwtComponent;

    @Autowired
    IpWhiteService ipWhiteService;

    @Autowired
    PaintingRecordService paintingRecordService;

    @GetMapping("/get/ai/server")
    public OpenResult getAiServer(HttpServletRequest request){
        String wId = jwtComponent.getUserWId(request);
        Integer count = aiDrawServerService.lambdaQuery()
                .eq(AiDrawServer::getServerType, 1)
                .eq(AiDrawServer::getWid, wId).count();
        if (count == 0){
            AiDrawServer aiDrawServer = new AiDrawServer();
            aiDrawServer.setServerName("个人服务器");
            aiDrawServer.setServerType(1);
            aiDrawServer.setServerCount(1);
            aiDrawServer.setServerIcon("<svg t=\"1682877803437\" class=\"icon\" viewBox=\"0 0 1024 1024\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" p-id=\"9645\" width=\"200\" height=\"200\"><path d=\"M409.6 597.333333v-166.4h0.580267a38.2976 38.2976 0 0 0-0.580267 6.4c0-41.591467 65.911467-77.687467 162.7136-96H61.576533A61.576533 61.576533 0 0 0 0 402.909867v201.096533A61.576533 61.576533 0 0 0 61.576533 665.6H409.6v-68.266667z\" fill=\"#424A60\" p-id=\"9646\"></path><path d=\"M410.180267 904.533333H409.6V665.6H61.576533A61.576533 61.576533 0 0 0 0 727.176533v201.096534A61.576533 61.576533 0 0 0 61.576533 989.866667h490.120534c-80.64-18.244267-135.645867-49.442133-141.5168-85.333334zM928.290133 341.333333H61.576533A61.5936 61.5936 0 0 1 0 279.7568V78.6432A61.5936 61.5936 0 0 1 61.576533 17.066667h866.696534A61.5936 61.5936 0 0 1 989.866667 78.6432v201.096533A61.5936 61.5936 0 0 1 928.290133 341.333333z\" fill=\"#556080\" p-id=\"9647\"></path><path d=\"M162.133333 179.2m-59.733333 0a59.733333 59.733333 0 1 0 119.466667 0 59.733333 59.733333 0 1 0-119.466667 0Z\" fill=\"#7383BF\" p-id=\"9648\"></path><path d=\"M836.266667 153.6m-17.066667 0a17.066667 17.066667 0 1 0 34.133333 0 17.066667 17.066667 0 1 0-34.133333 0Z\" fill=\"#7383BF\" p-id=\"9649\"></path><path d=\"M768 153.6m-17.066667 0a17.066667 17.066667 0 1 0 34.133334 0 17.066667 17.066667 0 1 0-34.133334 0Z\" fill=\"#7383BF\" p-id=\"9650\"></path><path d=\"M870.4 204.8m-17.066667 0a17.066667 17.066667 0 1 0 34.133334 0 17.066667 17.066667 0 1 0-34.133334 0Z\" fill=\"#7383BF\" p-id=\"9651\"></path><path d=\"M802.133333 204.8m-17.066666 0a17.066667 17.066667 0 1 0 34.133333 0 17.066667 17.066667 0 1 0-34.133333 0Z\" fill=\"#7383BF\" p-id=\"9652\"></path><path d=\"M699.733333 153.6m-17.066666 0a17.066667 17.066667 0 1 0 34.133333 0 17.066667 17.066667 0 1 0-34.133333 0Z\" fill=\"#7383BF\" p-id=\"9653\"></path><path d=\"M733.866667 204.8m-17.066667 0a17.066667 17.066667 0 1 0 34.133333 0 17.066667 17.066667 0 1 0-34.133333 0Z\" fill=\"#7383BF\" p-id=\"9654\"></path><path d=\"M631.466667 153.6m-17.066667 0a17.066667 17.066667 0 1 0 34.133333 0 17.066667 17.066667 0 1 0-34.133333 0Z\" fill=\"#7383BF\" p-id=\"9655\"></path><path d=\"M665.6 204.8m-17.066667 0a17.066667 17.066667 0 1 0 34.133334 0 17.066667 17.066667 0 1 0-34.133334 0Z\" fill=\"#7383BF\" p-id=\"9656\"></path><path d=\"M563.2 153.6m-17.066667 0a17.066667 17.066667 0 1 0 34.133334 0 17.066667 17.066667 0 1 0-34.133334 0Z\" fill=\"#7383BF\" p-id=\"9657\"></path><path d=\"M597.333333 204.8m-17.066666 0a17.066667 17.066667 0 1 0 34.133333 0 17.066667 17.066667 0 1 0-34.133333 0Z\" fill=\"#7383BF\" p-id=\"9658\"></path><path d=\"M162.133333 503.466667m-59.733333 0a59.733333 59.733333 0 1 0 119.466667 0 59.733333 59.733333 0 1 0-119.466667 0Z\" fill=\"#7383BF\" p-id=\"9659\"></path><path d=\"M162.133333 827.733333m-59.733333 0a59.733333 59.733333 0 1 0 119.466667 0 59.733333 59.733333 0 1 0-119.466667 0Z\" fill=\"#7383BF\" p-id=\"9660\"></path><path d=\"M716.8 832c-169.659733 0-307.2-48.708267-307.2-108.8V904.533333h0.580267c9.352533 57.105067 143.035733 102.4 306.619733 102.4s297.2672-45.294933 306.619733-102.4H1024V723.2c0 60.091733-137.540267 108.8-307.2 108.8zM409.6 716.8v6.4c0-2.1504 0.221867-4.283733 0.580267-6.4H409.6zM1023.419733 716.8c0.3584 2.116267 0.580267 4.2496 0.580267 6.4V716.8h-0.580267z\" fill=\"#1A9172\" p-id=\"9661\"></path><path d=\"M716.8 648.533333c-169.659733 0-307.2-48.708267-307.2-108.8V729.6h0.580267c9.352533 57.105067 143.035733 102.4 306.619733 102.4s297.2672-45.294933 306.619733-102.4H1024V539.733333C1024 599.825067 886.459733 648.533333 716.8 648.533333zM409.6 533.333333v6.4c0-2.1504 0.221867-4.283733 0.580267-6.4H409.6zM1023.419733 533.333333c0.3584 2.116267 0.580267 4.2496 0.580267 6.4v-6.4h-0.580267z\" fill=\"#25AE88\" p-id=\"9662\"></path><path d=\"M409.6 364.8a307.2 108.8 0 1 0 614.4 0 307.2 108.8 0 1 0-614.4 0Z\" fill=\"#88C057\" p-id=\"9663\"></path><path d=\"M716.8 473.6c-169.659733 0-307.2-48.708267-307.2-108.8V546.133333h0.580267c9.352533 57.105067 143.035733 102.4 306.619733 102.4s297.2672-45.294933 306.619733-102.4H1024V364.8c0 60.091733-137.540267 108.8-307.2 108.8zM409.6 358.4v6.4c0-2.1504 0.221867-4.283733 0.580267-6.4H409.6zM1023.419733 358.4c0.3584 2.116267 0.580267 4.2496 0.580267 6.4V358.4h-0.580267z\" fill=\"#61B872\" p-id=\"9664\"></path></svg>");
            aiDrawServer.setServerStatus(0);
            aiDrawServer.setCreateTime(new Date());
            aiDrawServer.setWid(Integer.parseInt(wId));
            aiDrawServerService.save(aiDrawServer);
        }
        List<AiDrawServer> aiDrawServer = aiDrawServerService.getAiDrawServer(wId);
        Map<String,Object> map = new HashMap<>();
        map.put("aiServerData",aiDrawServer);
        map.put("s_c_ck",Integer.parseInt(wId));
        return OpenResult.success(map);
    }


    @GetMapping("/get/user/search/friends/{toUser}")
    @Limit
    @RepeatSubmit(time = 3,msg = "3秒后再次搜索")
    public OpenResult searchFriends(@PathVariable String toUser){
        IpWhite ipWhite = ipWhiteService.lambdaQuery()
                .eq(IpWhite::getEmail, toUser).one();
        if (ipWhite == null){
            return OpenResult.error("没有搜索到当前好友");
        }
        if (ipWhite.getIpStatus() == 1 || ipWhite.getIpStatus() == 3){
            return OpenResult.error("当前好友状态不可被邀请");
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("nickname",ipWhite.getNikeName());
        jsonObject.set("username",ipWhite.getEmail().charAt(0)+"****"+ipWhite.getEmail().charAt(ipWhite.getEmail().length()-1));
        jsonObject.set("click_s",ipWhite.getWid());
        jsonObject.set("time",System.currentTimeMillis());
        return OpenResult.success(jsonObject);
    }

    @PostMapping("/create/server/on/submit")
    @Limit
    @RepeatSubmit
    public OpenResult createServerOnSubmit(@RequestBody JSONObject jsonObject,HttpServletRequest request){
        String wId = jwtComponent.getUserWId(request);
        if (StrUtil.isBlank(jsonObject.getStr("serverName"))){
            return OpenResult.error("缺少服务器名称");
        }
        if (jsonObject.getStr("serverName").length() > 15) {
            return OpenResult.error("服务器名字最大15个字符");
        }
        JSONArray friendsList = jsonObject.getJSONArray("friendsList");
        if (friendsList.size() == 0) {
            return OpenResult.error("至少需要一个好友");
        }

        if (aiDrawServerService.lambdaQuery()
                .eq(AiDrawServer::getWid,wId)
                .eq(AiDrawServer::getServerType,3).count() >= 5) {
            return OpenResult.error("自建服务器超出上限");
        }

        AiDrawServer aiDrawServer = new AiDrawServer();
        aiDrawServer.setServerName(jsonObject.getStr("serverName"));
        aiDrawServer.setServerType(3);
        aiDrawServer.setServerCount(friendsList.size() + 1);
        aiDrawServer.setServerIcon("<svg t=\"1685122297569\" class=\"icon\" viewBox=\"0 0 1024 1024\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" p-id=\"4358\" width=\"200\" height=\"200\"><path d=\"M447.5 960.3l-321-110v-642l321 82z\" fill=\"#37BBEF\" p-id=\"4359\"></path><path d=\"M447.5 960.3l450-240v-600l-450 170z\" fill=\"#2481BA\" p-id=\"4360\"></path><path d=\"M126.5 208.3l321 82 450-170-344.4-56.6z\" fill=\"#3ED6FF\" p-id=\"4361\"></path><path d=\"M400.3 348.9v80l-239-60v-80l239 60z m-239 48.3l239 60v-10l-239-60v10z m0 23l239 60v-10l-239-60v10z m0 23l239 60v-10l-239-60v10z\" fill=\"#D4F7FF\" p-id=\"4362\"></path><path d=\"M175.7 308.4v34l210.2 53v-34zM175.7 363.4l210.2 53v-13.3l-210.2-53z\" fill=\"#113B42\" p-id=\"4363\"></path><path d=\"M447.5 894.1v-5.2l-174.4-55.3-1.2 4.8zM271.9 809.3L447.5 864v-5.3l-174.4-54.3zM271.9 780.1l175.6 53.7v-5.2l-174.4-53.4zM447.5 707.9l-174.4-49.3-1.2 4.8 175.6 49.7zM271.9 692.6l175.6 50.7v-5.2l-174.4-50.4zM271.9 750.9l175.6 52.7v-5.2l-174.4-52.3zM271.9 721.8l175.6 51.7v-5.3l-174.4-51.3z\" fill=\"#0071BC\" p-id=\"4364\"></path><path d=\"M379.5 540.8c-9.4-3.4-17 1.6-17 11.2s7.6 20.1 17 23.2 17-1.9 17-11.2-7.6-19.8-17-23.2zM379.5 603.6c-3.9-1.2-7 1-7 4.9s3.1 8.1 7 9.3 7-1 7-4.9c0-3.9-3.1-8-7-9.3z\" fill=\"#FFFFFF\" p-id=\"4365\"></path></svg>");
        aiDrawServer.setServerStatus(0);
        aiDrawServer.setCreateTime(new Date());
        aiDrawServer.setWid(Integer.parseInt(wId));
        StringBuilder stringBuilder = new StringBuilder();
        for (Object o : friendsList) {
            JSONObject obj = (JSONObject)o;
            if (StrUtil.isBlank(stringBuilder.toString())){
                stringBuilder.append(obj.getInt("click_s"));
            }else {
                stringBuilder.append(",").append(obj.getInt("click_s"));
            }
        }
        stringBuilder.append(",").append(wId);
        aiDrawServer.setCreateWidList(stringBuilder.toString());
        boolean save = aiDrawServerService.save(aiDrawServer);
        return OpenResult.save(save);
    }

    @PostMapping("/ai/draw/delete/server")
    public OpenResult deleteServer(@RequestBody JSONObject jsonObject, HttpServletRequest request){
        if (jsonObject.getInt("sid") == null) {
            return OpenResult.error("参数错误");
        }
        String wId = jwtComponent.getUserWId(request);
        if (aiDrawServerService.lambdaQuery()
                .eq(AiDrawServer::getWid,wId)
                .eq(AiDrawServer::getSid,jsonObject.getInt("sid"))
                .eq(AiDrawServer::getServerType,3).count() == 0) {
            return OpenResult.error("删除错误");
        }else {
            paintingRecordService.lambdaUpdate()
                    .eq(PaintingRecord::getSid, jsonObject.getInt("sid")).remove();
            boolean remove = aiDrawServerService.removeById(jsonObject.getInt("sid"));
            return OpenResult.delete(remove);
        }
    }

}
