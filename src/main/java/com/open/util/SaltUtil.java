package com.open.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.Key;

/**
 * @author page-view
 * <p>des</p>
 **/
public class SaltUtil {

    //properties
    private static final String[] SALT_ARRAY = {
            "A","P","C","G","H","Z","j","g","t","o","a","b",
            "0","1","2","3","4","5","6","7","8","9","l","b",
            "Z","X","C","B","H","S","j","I","t","o","W","b",
            "w","1","","3","4","w","o","7","f","a","j","k",
    };


    public static String decryptDouble(String str){
        StringBuilder builder = new StringBuilder();
            char[] toCharArray = str.toCharArray();
            for (int i = 0; i < toCharArray.length; i++) {
                if (i % 2 == 0){
                    builder.append(toCharArray[i]);
                }
            }
        return builder.toString();
    }

}
