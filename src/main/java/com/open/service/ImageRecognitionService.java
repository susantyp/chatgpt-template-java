package com.open.service;

import com.open.bean.ImageRecognition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【image_recognition】的数据库操作Service
* @createDate 2023-07-27 14:35:23
*/
public interface ImageRecognitionService extends IService<ImageRecognition> {

}
