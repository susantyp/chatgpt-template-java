package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.OpenDict;
import com.open.service.OpenDictService;
import com.open.mapper.OpenDictMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【open_dict】的数据库操作Service实现
* @createDate 2023-03-19 00:34:13
*/
@Service
public class OpenDictServiceImpl extends ServiceImpl<OpenDictMapper, OpenDict>
    implements OpenDictService{

    @Override
    public String getDict(String code) {
        OpenDict blogDict = lambdaQuery().eq(OpenDict::getDictCode, code).one();
        return blogDict.getDictValue();
    }

    @Override
    public void setDict(String code,Object obj) {
        lambdaUpdate()
                .set(OpenDict::getDictValue,obj)
                .eq(OpenDict::getDictCode,code).update();
    }

    @Override
    public void subtractDict(String code, Object obj) {
        String dict = getDict(code);
        setDict(code,Integer.parseInt(dict) - 1);
    }

}




