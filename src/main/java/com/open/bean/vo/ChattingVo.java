package com.open.bean.vo;

import com.open.bean.RequestRecord;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Data
public class ChattingVo {

    private Long groupCode;

    private List<RequestRecordVo> requestRecordVoList = new ArrayList<>();

    private String date;

    public ChattingVo(Long groupCode, List<RequestRecord> requestRecordList,String date) {
        this.groupCode = groupCode;
        this.date = date;
        for (RequestRecord record : requestRecordList) {
            requestRecordVoList.add(new RequestRecordVo(record.getRequestContent(),record.getRespondContent(),record.getRequestId(),
                    record.getSystemName(),record.getSystemRid(),record.getModelType()));
        }
    }

    public ChattingVo() {
    }
}
