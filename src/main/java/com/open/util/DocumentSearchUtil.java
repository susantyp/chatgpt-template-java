package com.open.util;

import cn.hutool.core.io.FileUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * @author typsusan
 * <p>des</p>
 **/
public class DocumentSearchUtil {
    private Set<String> selectedTitles = new HashSet<>();
    private String currentTitle = null;
    private boolean foundCurrentTitle = false;

    public void processDocument(String document, String keyword) {
        String[] lines = document.split("\r\n");
        for (String line : lines) {
            if (isTitle(line)) {
                foundCurrentTitle = true;
                currentTitle = line;
                selectedTitles.add(currentTitle);
                processParagraph(line, keyword); // 处理标题本身作为段落
            } else if (foundCurrentTitle) {
                if (shouldBreak(line)) {
                    foundCurrentTitle = false;
                    currentTitle = null;
                } else {
                    processParagraph(line, keyword);
                }
            }
        }
    }

    public void processParagraph(String paragraph, String keyword) {
        if (currentTitle != null && !isDuplicate(paragraph) && paragraph.contains(keyword)) {
            System.out.println("段落：" + paragraph);
        }
    }

    public boolean isTitle(String line) {
        // 根据实际情况判断是否为标题
        // 这里可以使用正则表达式或其他方法来判断
        return line.matches("^(\\([\\d]+\\)|[\\d]+\\.)\\s.*");
    }

    public boolean shouldBreak(String line) {
        // 根据实际情况判断是否需要截断段落
        // 如果遇到括号开头的标题或数字开头的标题，就返回 true
        return isTitle(line);
    }

    public boolean isDuplicate(String paragraph) {
        return selectedTitles.contains(paragraph);
    }


    public static void main(String[] args) {
        DocumentSearchUtil documentSearch = new DocumentSearchUtil();
        documentSearch.processDocument(FileUtil.readUtf8String("C:\\Users\\typsusan\\Desktop\\test.txt"),
                "如果是大括号内为空，");
    }
}
