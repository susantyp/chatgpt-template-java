//package com.open.controller;
//
//import cn.hutool.core.util.StrUtil;
//import com.open.bean.FileCache;
//import com.open.component.JwtComponent;
//import com.open.result.OpenResult;
//import com.open.service.FileCacheService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.Objects;
//import java.util.UUID;
//
///**
// * @author typsusan
// * <p>des</p>
// **/
//@RestController
//@RequestMapping("/request/open")
//@Slf4j
//public class FileUploadController {
//
//    @Autowired
//    FileCacheService fileCacheService;
//
//    @Autowired
//    JwtComponent jwtComponent;
//
//    private static final long MAX_FILE_SIZE = 3 * 1024 * 1024;
//
//    // 允许上传的文件格式
//    private static final String[] ALLOWED_FORMATS = {"pdf"};
//
//    @PostMapping("/file/upload/{code}")
//    public OpenResult upload(@PathVariable String code, @RequestParam("file") MultipartFile file, HttpServletRequest request) {
//        try {
//            String wId = jwtComponent.getUserWId(request);
//            String uploadPath = "/data/chat/file/";
//
//            // 获取文件格式
//            String fileFormat = getFileExtension(Objects.requireNonNull(file.getOriginalFilename()));
//            // 判断文件格式是否允许
//            if (!isAllowedFormat(fileFormat)) {
//                return OpenResult.error("仅允许上传：" + String.join(", ", ALLOWED_FORMATS) + " 格式的文件（其他格式正在开发中。。。）");
//            }
//            // 判断文件大小是否超出限制
//            if (file.getSize() > MAX_FILE_SIZE) {
//                return OpenResult.error("超出" + MAX_FILE_SIZE + "MB");
//            }
//            String fileName = UUID.randomUUID().toString() + "." + fileFormat;
//            String resultFile = uploadPath + fileName;
//            File destFile = new File(resultFile);
//            file.transferTo(destFile);
//            FileCache fileCache = new FileCache();
//            fileCache.setMsgCode(code);
//            if (fileFormat.equals("pdf")){
//                fileCache.setFileText(readPdf(resultFile));
//                if (fileCache.getFileText().length() > 4000){
//                    return OpenResult.error("文档内容不允许超过4000个字符");
//                }
//            }
//
//            fileCache.setFileType(fileFormat);
//            fileCache.setFilePath(resultFile);
//            fileCache.setWid(Integer.parseInt(wId));
//            fileCache.setCreateTime(new Date());
//            fileCacheService.save(fileCache);
//            if (StrUtil.isNotBlank(fileCache.getFileText())){
//                destFile.delete();
//            }
//            return OpenResult.success();
//        } catch (IOException e) {
//            e.printStackTrace();
//            return OpenResult.error("上传失败");
//        }
//    }
//
//    private String getFileExtension(String fileName) {
//        int dotIndex = fileName.lastIndexOf('.');
//        if (dotIndex > 0) {
//            return fileName.substring(dotIndex + 1).toLowerCase();
//        }
//        return "";
//    }
//
//    private boolean isAllowedFormat(String fileFormat) {
//        return Arrays.asList(ALLOWED_FORMATS).contains(fileFormat);
//    }
//
//    public static String readPdf(String path){
//        PDDocument document = null;
//        try {
//            document = PDDocument.load(new File(path));
//            PDFTextStripper pdfTextStripper = new PDFTextStripper();
//            return "你现在是一个文档读取机器，文档内容如下，我将会以文档内容跟你进行交互 \n"
//                    + pdfTextStripper.getText(document);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }finally {
//            try {
//                if (document != null){
//                    document.close();
//                }
//            }catch (IOException e){
//                e.printStackTrace();
//            }
//        }
//    }
//
//
//}
